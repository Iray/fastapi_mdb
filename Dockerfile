FROM python:3.10-slim

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt

COPY ./README.md /code/README.md

COPY ./pytest.ini /code/pytest.ini

RUN python3 -m venv /code/opt/venv

RUN . /code/opt/venv/bin/activate && pip install -r /code/requirements.txt

COPY ./src /code/src

CMD . /code/opt/venv/bin/activate && exec python -m src.app.main
