from pydantic import BaseSettings, Field, SecretStr


class Settings(BaseSettings):
    colletion_name: str = "texts"
    mongo_url: SecretStr = Field(..., env="MONGO_URL")
    mongo_url_test: SecretStr = Field(..., env="MONGO_URL_TEST")
    database_name: SecretStr = Field(..., env="DATABASE_NAME")

    class Config:
        env_prefix = ""
        case_sentive = False
        env_file = "src/.env"
        env_file_encoding = "utf-8"
