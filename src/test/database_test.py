import asyncio

import pytest
from bson.errors import InvalidId

test_textHelper_params_correct = [
    (
        {"_id": "0121245sss", "text": "jakis tekst"},
        {"id": "0121245sss", "text": "jakis tekst"},
    ),
    ({"_id": "121245sss", "text": 100}, {"id": "121245sss", "text": 100}),
]

test_textHelper_params_incorrect = [
    (
        {"_id": "0121245sss", "tex": "jakis tekst"},
        {"id": "0121245sss", "text": "jakis tekst"},
    ),
    ({"_i": "121245sss", "text": 100}, {"id": "121245sss", "text": 100}),
]

test_addText_params_correct = [
    ({"text": "some text"}),
    ({"text": 1212122}),
    ({"text": 1212122}),
]

test_addText_params_incorrect = [
    ({"tex": "some text"}),
    ({"od": 1212122}),
    ({"zw": 1212122}),
]


pytestmark = pytest.mark.asyncio


@pytest.mark.parametrize("input,output", test_textHelper_params_correct)
async def test_textHelper_correctReturnedValue(database_caller, input, output):
    assert database_caller.textHelper(input) == output


@pytest.mark.parametrize("input,output", test_textHelper_params_incorrect)
async def test_textHelper_IncorrectReturnedValue(database_caller, input, output):
    with pytest.raises(KeyError):
        database_caller.textHelper(input) == output


@pytest.mark.parametrize("input", test_addText_params_correct)
async def test_addText_successfullAddingText(database_caller, input):

    database_caller.collection_name = "test_collection"
    database_caller.text_collection = database_caller.database_hook[
        database_caller.collection_name
    ]
    result = await database_caller.addText(input)
    assert result["text"] == input["text"]


@pytest.mark.parametrize("input", test_addText_params_incorrect)
async def test_addText_successfullAddingText(database_caller, input):

    database_caller.collection_name = "test_collection"
    database_caller.text_collection = database_caller.database_hook[
        database_caller.collection_name
    ]
    with pytest.raises(KeyError):
        result = await database_caller.addText(input)
        assert result["text"] == input["text"]


async def test_retriveText_correctIdAndText(database_caller, populateDatabase):
    database_caller.collection_name = "test_collection"
    database_caller.text_collection = database_caller.database_hook[
        database_caller.collection_name
    ]
    result = await database_caller.retriveText(populateDatabase["id"])
    assert result["id"] == populateDatabase["id"]


async def test_retriveText_idNoExists(database_caller, populateDatabase):
    database_caller.collection_name = "test_collection"
    database_caller.text_collection = database_caller.database_hook[
        database_caller.collection_name
    ]
    test_id = "aaaaaaaaaaaaaaaaaaaaaaaa"
    result = await database_caller.retriveText(test_id)
    assert result is None


async def test_retriveText_incorrectId(database_caller, populateDatabase):
    database_caller.collection_name = "test_collection"
    database_caller.text_collection = database_caller.database_hook[
        database_caller.collection_name
    ]
    test_id = "aaa"
    with pytest.raises(InvalidId):
        result = await database_caller.retriveText(test_id)
