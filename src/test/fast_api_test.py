import pytest
from bson.errors import InvalidId
from httpx import AsyncClient

from src.app.server.app import app
from src.app.server.database.database import Database
from src.app.server.depands_methods.database_depends import (getDatabase,
                                                             getTestDatabase)

pytestmark = pytest.mark.asyncio


async def test_getTextData_correctID(populateDatabase):
    app.dependency_overrides[getDatabase] = getTestDatabase
    test_id = populateDatabase["id"]
    async with AsyncClient(
        app=app,
        base_url="http://test/",
    ) as ac:
        response = await ac.get(test_id)
    response_json = response.json()
    res_data = response_json["data"]
    assert response.status_code == 200
    assert res_data[0].get("id") == populateDatabase["id"]


async def test_getTextData_idNoExists():
    app.dependency_overrides[getDatabase] = getTestDatabase
    test_id = "aaaaaaaaaaaaaaaaaaaaaaaa"
    async with AsyncClient(
        app=app,
        base_url="http://test/",
    ) as ac:
        response = await ac.get(test_id)
    response_json = response.json()
    assert response_json["code"] == 404


async def test_getTextData_incorrectExists():
    app.dependency_overrides[getDatabase] = getTestDatabase
    test_id = "aa"
    with pytest.raises(InvalidId):
        async with AsyncClient(
            app=app,
            base_url="http://test/",
        ) as ac:
            await ac.get(test_id)


async def test_postAddText_correctData():
    app.dependency_overrides[getDatabase] = getTestDatabase
    text = {"text": "something to insert"}
    async with AsyncClient(
        app=app,
        base_url="http://test/",
    ) as ac:
        response = await ac.post(url="", json=text)
    res_json = response.json()
    res_data = res_json["data"]
    print(res_data[0])
    assert response.status_code == 200
    assert res_data[0].get("text") == text["text"]


async def test_postAddText_incorrectKey():
    app.dependency_overrides[getDatabase] = getTestDatabase
    text = {"xyz": "something to insert"}
    with pytest.raises(KeyError):
        async with AsyncClient(
            app=app,
            base_url="http://test/",
        ) as ac:
            response = await ac.post(url="", json=text)
        res_json = response.json()
        res_data = res_json["data"]
        print(res_data[0])
        assert response.status_code == 200
        assert res_data[0].get("text") == text["text"]
