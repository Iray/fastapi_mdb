import asyncio

import pytest

from src.app.server.database.database import Database as db


@pytest.fixture()
async def database_caller(event_loop):
    test_db = db(event_loop=event_loop)
    yield test_db
    test_db.collection_name = "test_collection"
    test_db.database_hook.drop_collection(test_db.collection_name)


@pytest.fixture
async def populateDatabase(database_caller):
    test_text = {"text": "abc"}

    database_caller.collection_name = "test_collection"
    database_caller.text_collection = database_caller.database_hook[
        database_caller.collection_name
    ]
    source_dict = await database_caller.addText(test_text)
    return source_dict
