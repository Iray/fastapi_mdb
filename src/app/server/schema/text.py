from pydantic import BaseModel, Field


class TextSchema(BaseModel):
    """Text containg model of Text Colletion element

    Args:
        BaseModel BaseModel: Inherited from Pydanic BaseModel
    """

    text: str = Field(...)

    class Config:
        """Class containg example of dict for creating correct model"""

        schema_extra = {"example": {"text": "something to insert into MongoDB"}}
