from src.app.server.database.database import Database as db


async def getDatabase():
    """Dependency for endpoinsts functions

    Returns:
        Database: instace of Database
    """
    database = db()
    return database


async def getTestDatabase():
    """Dependency for testing endpoinsts functions

    Returns:
        Database: instace of test Database
    """
    database = db()
    database.collection_name = "test_collection"
    database.text_collection = database.database_hook[database.collection_name]
    return database
