import motor.motor_asyncio
from bson.objectid import ObjectId

from src.config import Settings


class Database:
    """Class for comunicating with MongoDB"""

    my_settings = Settings()
    mongo_details = my_settings.mongo_url.get_secret_value()
    database_name = my_settings.database_name.get_secret_value()
    collection_name = my_settings.colletion_name

    def __init__(self, event_loop=None):
        if event_loop is not None:
            self.client = motor.motor_asyncio.AsyncIOMotorClient(
                self.mongo_details, io_loop=event_loop
            )
        else:
            self.client = motor.motor_asyncio.AsyncIOMotorClient(self.mongo_details)

        self.database_hook = self.client[self.database_name]
        self.text_collection = self.database_hook[self.collection_name]

    def textHelper(self, text) -> dict:
        """Converting element of TextCollection into dict
        Args:
            text dict: element of MongoDB collection

        Returns:
            dict: data from given dict
        """
        return {
            "id": str(text["_id"]),
            "text": (text["text"]),
        }

    async def retriveText(self, id: str) -> dict:
        """Getting one element of Text Collection

        Args:
            id (str): MongoDB id of one element from Text Collection

        Returns:
            dict: Got Text Collection element as dict
        """
        single_text = await self.text_collection.find_one({"_id": ObjectId(id)})
        if single_text:
            return self.textHelper(single_text)

    async def addText(self, text_data: dict) -> dict:
        """Adding one element to Text Collection

        Args:
            text_data (dict): content of new Text Collection element

        Returns:
            dict: Data from briefly created element of Text Collection
        """
        single_text = await self.text_collection.insert_one(text_data)
        new_text = await self.text_collection.find_one({"_id": single_text.inserted_id})
        return self.textHelper(new_text)
