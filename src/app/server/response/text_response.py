class TextResponse:
    """Class for Text Colletion Responses"""

    def responseModel(data, message):
        """Returning dict with informations from sucessfull inserting element into Text Colletion

        Args:
            data dict: Id and text from element of Text Colletion
            message str: Feedback

        Returns:
            dict: Values from Text Colletion element, https code and message with feedback
        """
        return {
            "data": [data],
            "code": 200,
            "message": message,
        }

    def errorResponseModel(error, code, message):
        """Response dict with information from failure during inserting element into Text Colletion

        Args:
            error str: Ocured Error
            code int: Https response code
            message str: Feedback

        Returns:
            dict: Contain error, code and message
        """
        return {"error": error, "code": code, "message": message}
