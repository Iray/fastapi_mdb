from fastapi import Body, Depends, FastAPI
from fastapi.encoders import jsonable_encoder

from src.app.server.database.database import Database
from src.app.server.depands_methods.database_depends import getDatabase
from src.app.server.response.text_response import TextResponse
from src.app.server.schema.text import TextSchema

description = open("README.md","r").read() 



app = FastAPI(
    title="FastAPI with MongoDB",
    description=description,
    version="1.0.0",
    contact={"GitLAB": "gitlab.com/Iray"},
)


@app.post(
    "/",
    response_description="Text data added into the database",
    description="Endpoint for adding text into MongoDB",
    name="Insert text",
    tags=["Endpoint"],
)
async def post_add_text(
    text: TextSchema = Body(...), database: Database = Depends(getDatabase)
):
    """Adding new element to Text Colletion

    Args:
        text (TextSchema, optional): Given text from request body. Defaults to Body(...).

    Returns:
        dict: Information about success or failure of insert operation
    """

    temp_text = jsonable_encoder(text)
    new_text = await database.addText(temp_text)
    return TextResponse.responseModel(new_text, "Text added successfully")


@app.get(
    "/{id}",
    response_description="Single text data retrieved",
    description="Endpoint for getting single text from MongoDB",
    name="Get text",
    tags=["Endpoint"],
)
async def get_text_data(id, database: Database = Depends(getDatabase)):
    """Getting one element from Text Colletion using id of specific element

        Args:
            id str: specific element id from Text Colletion

        Returns:
    dict: Information about success or failure of add operation"""
    text = await database.retriveText(id)
    if text:
        return TextResponse.responseModel(
            text, "Single text data retrieved successfully"
        )
    return TextResponse.errorResponseModel(
        "An error occurred.", 404, "text doesn't exist."
    )
