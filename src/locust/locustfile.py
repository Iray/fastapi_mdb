import random

from locust import HttpUser, between, task


class AppUser(HttpUser):
    """Class for load tests with Locust

    Args:
        HttpUser (HttpUser): Inherited from Locust class
    """

    wait_time = between(0, 2.5)

    host = "http://0.0.0.0:8080"
    my_texts = [
        "Welcome to hell",
        "Mistrz przerósł ucznia",
        "dżentelmeni metalu",
        "na siedmiu wzgórzach piętrzy sie Rzym",
    ]

    @task
    def textPostPage(self):
        """Simple task for testing "/" endpoint aka inserting new values to database"""
        mytext = random.choice(self.my_texts)
        self.client.post("/", json={"text": str(mytext)})
