# FastAPI_MongoDB
Simple microservice with two endpoints, using FastAPI and MongoDB, which is conterized with Docker. 

## Table of Contents
- [FastAPI_MongoDB](#fastapi_mongodb)
  - [Table of Contents](#table-of-contents)
  - [General Information](#general-information)
  - [Technologies Used](#technologies-used)
  - [Features](#features)
  - [Setup](#setup)
  - [Usage](#usage)
  - [Project Status](#project-status)
  - [Contact](#contact)

## General Information
- This project was created as interview task
- First endpoint ("/") letting user post text to database and second ("/{id}") to get text if the given id is valid.   
- Learn more about microservices using FastAPI, Docker, MongoDB,unit and load testing practice


## Technologies Used
Look into requirements.txt


## Features
List the ready features here:
- Inserting into database some text
- Getting text stored in database when correct id was given
- Unit test with pytest and load test with locust
- FastApi docs 


## Setup
Requirements are in file requirements.txt. Located in main folder.

Create virtualenv with python 3.10 in cloned repo:
```python

python3 -m venv /path/to/cloned/repo

```

Go into repo folder then use correct method for your case [how to activate venv](https://docs.python.org/3/library/venv.html). Then use:
`pip install -r requirements.txt`.


 In main folder create .env file for docker compose, containg for example:
 ```.env
MONGO_INITDB_ROOT_USERNAME: root
MONGO_INITDB_ROOT_PASSWORD: example
 ```
 In src folder create .env file for settings, containg for example:
 ```.env
MONGO_URL = "mongodb://root:example@db:27017/"
DATABASE_NAME = "texts"
MONGO_URL_TEST="mongodb://localhost:27017"
 ```


## Usage
If you want to use the code locally, run the tests, line 10 in file fastapi_mdb/src/app/server/database/database.py should look like this:
`mongo_details = my_settings.mongo_url_test.get_secret_value()`. 

If a docker compose is used, this line should look like this:
`mongo_details = my_settings.mongo_url.get_secret_value()`.

Runnig locally:
<b>Remeber to read the beginning of the paragraph</b>

In terminal go into folder fastapi_mdb. Then: `python -m src.app.main`. Then in your browser write: `0.0.0.0:8080`

Running unit tests:
In terminal go into folder fastapi_mdb. Then: `pytest .`

Running load test:
In terminal go into folder fastapi_mdb. Then: `python -m src.app.main`. Now go into locust folder and here: `locust -f locustfile -r 2 -u 20 --autostart --logfile ./log.txt --csv my_csv -t 20s --autoquit 5`.

Running with docker:
<b>Remeber to read the beginning of the paragraph</b>

In terminal go into folder fastapi_mdb. Then: `docker compose up --build`. Then in your browser write: `0.0.0.0:8080`

Manual tests:
When app is up then go to: `0.0.0.0:8080/docs`. Docs will guide you

## Project Status
Project is:  _complete_ . 


## Contact
In any case leave a comment or something on repo.

